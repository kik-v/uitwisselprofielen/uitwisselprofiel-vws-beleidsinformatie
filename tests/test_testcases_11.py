from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK ((Vest. 1254)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Geen ZVL functie + Geen AOK ((Vest. 1287)))",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ]
    }
]

td_01_b = [
    {
        "Description": "Testcase 01b (Geen ZVL functie + Geen AOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk", 
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK (Vest. 1254))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Geen ZVL functie + Wel WOK (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_Grotestraat",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat", 
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen WOK (Vest. 1254))",
        "Amount": 10, 
        "Human": [
            {
                "VrijwilligersOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH. OK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_De_Beuk",                      
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_De_Beuk", 
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03_a = [
    {
        "Description": "Testcase 03a (Wel ZVL functie + Geen WOK (Vest. 1287))",
        "Amount": 10, 
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH. OK Geen ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                      
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]


td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel WOK (AOK) (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel WOK (INH OK) (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",
                             }
                        ]
                    },
                    
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel WOK (Oproep OK) (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "OproepOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",
                             }
                        ]
                    },
                    
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel WOK (UITZ OK) (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "UitzendOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",
                             }
                        ]
                    },
                    
                ]
            }
        ]
    }
]

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel WOK (AOK OBPT) (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",
                             }
                        ]
                    },
                    
                ]
            }
        ]
    }
]

td_04_e = [
    {
        "Description": "Testcase 04e (Wel ZVL functie + Wel WOK (AOK BPT) (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",
                             }
                        ]
                    },
                    
                ]
            }
        ]
    }
]

td_04_f = [
    {
        "Description": "Testcase 04f (Wel ZVL functie + Wel WOK (AOK BBL) (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomstBBL": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Vestiging_Grotestraat",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",
                             }
                        ]
                    },
                    
                ]
            }
        ]
    }
]

#Static Tests
def test_if_headers_are_correct_for_query_1_1_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

    test.set_reference_date_to("2024-01-01")

    # Assertions
    test.verify_header_present('Vestiging')
    test.verify_header_present('Zorg')
    test.verify_header_present('Niet_zorg')
    test.verify_header_present('Totaal')

def test_if_number_of_rows_returned_is_correct_for_query_1_1_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

    test.set_reference_date_to("2024-01-01")

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_1_1_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')


    test.set_reference_date_to("2024-01-01")

    # Assertions
    test.verify_value("Zorg", "9",where_condition=("Vestiging","000001254"))
    # test.verify_value("Niet_zorg", "2",where_condition=("Vestiging","000001254"))
    # test.verify_value("Totaal", "10",where_condition=("Vestiging","000001254"))

    # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
    # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001287"))
    # test.verify_value("Totaal", "3",where_condition=("Vestiging","000001287"))

    # test.verify_value("Zorg", "10",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))
    # test.verify_value("Niet_zorg", "3",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))
    # test.verify_value("Totaal", "13",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))

def test_if_dates_can_change_1_1_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

    test.set_reference_date_to("2023-01-01")
    # test.change_reference_date_to("2022-01-01", "2024-01-01")

    # Assertions
    # test.verify_value("Zorg", "9",where_condition=("Vestiging","000001254"))
    # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
    # test.verify_value("Totaal", "10",where_condition=("Vestiging","000001254"))

    # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
    # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001287"))
    # test.verify_value("Totaal", "3",where_condition=("Vestiging","000001287"))

    # test.verify_value("Zorg", "11",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))
    # test.verify_value("Niet_zorg", "2",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))
    test.verify_value("Totaal", "13",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_1_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen WOK- client (Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        test.verify_value("Totaal", "13",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_1_0_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie + Geen WOK - client (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        test.verify_value("Totaal", "13",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01b
def test_if_value_returned_is_correct_for_query_1_1_0_01_b(db_config):
    """ Testcase 01b (Geen ZVL functie + Geen WOK (ST OK) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        test.verify_value("Totaal", "13",where_condition=("Vestiging","Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_1_1_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel WOK (Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001254"))
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_1_1_0_02_a(db_config):
    """ Testcase 02a (Geen ZVL functie + Wel WOK (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001287"))
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_1_1_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen WOK (ST OK) (Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        test.verify_value("Zorg", "9",where_condition=("Vestiging","000001254"))
        # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
        # test.verify_value("Totaal", "10",where_condition=("Vestiging","000001254"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_1_1_0_03_a(db_config):
    """ Testcase 03a (Wel ZVL functie + Geen WOK (VWOK) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001287"))
        # test.verify_value("Totaal", "3",where_condition=("Vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_1_1_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel WOK (AOK) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
        # test.verify_value("Zorg", "12",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "2",where_condition=("Vestiging","000001287"))
        test.verify_value("Totaal", "13",where_condition=("Vestiging","000001287"))
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_1_1_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel WOK (INH OK) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
#         test.verify_value("Zorg", "19",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Totaal", "20",where_condition=("Vestiging","000001254"))

        # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001287"))
        test.verify_value("Totaal", "13",where_condition=("Vestiging","000001287"))
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_1_1_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel WOK (OP OK) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
#         test.verify_value("Zorg", "19",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Totaal", "20",where_condition=("Vestiging","000001254"))

        # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001287"))
        test.verify_value("Totaal", "13",where_condition=("Vestiging","000001287"))
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_1_1_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel WOK (UITZ OK)(Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
#         test.verify_value("Zorg", "19",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Totaal", "20",where_condition=("Vestiging","000001254"))

        # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001287"))
        test.verify_value("Totaal", "13",where_condition=("Vestiging","000001287"))
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d
def test_if_value_returned_is_correct_for_query_1_1_0_04_d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel WOK (AOK OBPT) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
#         test.verify_value("Zorg", "19",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Totaal", "20",where_condition=("Vestiging","000001254"))

        # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001287"))
        test.verify_value("Totaal", "13",where_condition=("Vestiging","000001287"))
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04e
def test_if_value_returned_is_correct_for_query_1_1_0_04_e(db_config):
    """ Testcase 04e (Wel ZVL functie + Wel WOK (AOK BPT) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
#         test.verify_value("Zorg", "19",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Totaal", "20",where_condition=("Vestiging","000001254"))

        # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001287"))
        test.verify_value("Totaal", "13",where_condition=("Vestiging","000001287"))
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04f
def test_if_value_returned_is_correct_for_query_1_1_0_04_f(db_config):
    """ Testcase 04f (Wel ZVL functie + Wel WOK (AOKBBL) (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.1. Aantal personeelsleden op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")
        # test.change_reference_date_to("2022-01-01", "2024-01-01")

        # Verify actual result of the query
#         test.verify_value("Zorg", "19",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Niet_zorg", "1",where_condition=("Vestiging","000001254"))
#         # test.verify_value("Totaal", "20",where_condition=("Vestiging","000001254"))

        # test.verify_value("Zorg", "2",where_condition=("Vestiging","000001287"))
        # test.verify_value("Niet_zorg", "11",where_condition=("Vestiging","000001287"))
        test.verify_value("Totaal", "13",where_condition=("Vestiging","000001287"))
    
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()