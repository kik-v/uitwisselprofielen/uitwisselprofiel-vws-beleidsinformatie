from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-01-01

# Opmerkingen:

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK BBL (Vest. 1254))",
        "Amount": 10, #Indicator score: 0
        "Human": [
             {
                 "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + AOK BBL (Vest. 1254))",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomstBBL": [
                    {
                        "function": [
                            {
                                "label": "AOK BBL Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK BBL (Vest. 1254))",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK BBL (12m, Vest. 1254))",
        "Amount": 10, #Indicator score: 10
        "Human": [
            {
                "ArbeidsOvereenkomstBBL": [
                    {
                        "function": [
                            {
                                "label": "AOK BBL Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK BBL (12m, Vest. 1287))",
        "Amount": 10, #Indicator score: 10
        "Human": [
            {
                "ArbeidsOvereenkomstBBL": [
                    {
                        "function": [
                            {
                                "label": "AOK BBL Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]


#Static Tests
def test_if_headers_are_correct_for_query_1_6_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

        test.set_reference_date_to("2024-01-01")

        # Assertions
        test.verify_header_present('Vestiging')
        test.verify_header_present('Aantal_BBL')
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

def test_if_number_of_rows_returned_is_correct_for_query_1_6_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_row_count(0)

def test_if_indicator_has_correct_value_for_query_1_6_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

    test.set_reference_date_to("2022-01-01")

    # Assertions
    test.verify_value("Aantal_BBL", None)
    #.in_row(1)

def test_if_dates_can_change_1_6_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

    test.set_reference_date_to("2023-01-01")

    # Assertions
    test.verify_value("Aantal_BBL", None)

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_6_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK BBL (Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Aantal_BBL", None)
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 02
def test_if_value_returned_is_correct_for_query_1_6_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK BBL (Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Aantal_BBL", "10", where_condition=("Vestiging","000001254"))
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 03
def test_if_value_returned_is_correct_for_query_1_6_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK BBL (Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Aantal_BBL", None)
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 04
def test_if_value_returned_is_correct_for_query_1_6_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK BBL (12m, Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Aantal_BBL", "10", where_condition=("Vestiging","000001254"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


#Testcase 04a
def test_if_value_returned_is_correct_for_query_1_6_0_04a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK BBL (6m tijdens meetperiode, Vest. 1254))
        VWS Beleidsontwikkeling & -monitoring 1.6.0. Aantal leerlingen op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.6.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-01-01")

        # Verify actual result of the query
        test.verify_value("Aantal_BBL", "10", where_condition=("Vestiging","000001287"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
