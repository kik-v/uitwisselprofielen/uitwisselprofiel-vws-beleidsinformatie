from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-12-31

#Opmerkingen:

#Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK/INH/UITZ )",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Locatie_De_Beuk_1",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK )",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK )",
        "Amount": 10, # Indicator score =  0.212184629694221602139290 (10 * 36/36 = 10. 10/47 = 0.212...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Locatie_De_Beuk_1",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK (8u inzet, Vest. 1287))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",                      
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_Grotestraat_17"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK + Wel INH OK (8u + 8u inzet))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "location": "Locatie_De_Beuk_1",                      
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "location": "Locatie_De_Beuk_1",                        
                        "size": [
                             {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel INH OK (8u inzet)",
        "Amount": 10, 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "location": "Locatie_De_Beuk_1",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel UTZ OK + Wel INH OK (8u + 8u inzet))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "UitzendOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",   
                        "location": "Locatie_De_Beuk_1",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",     
                        "location": "Locatie_De_Beuk_1",                     
                        "size": [
                             {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00",
                                "location": "Locatie_De_Beuk_1"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 



#Static Tests

def test_if_headers_are_correct_for_query_1_4_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')

    # test.set_quarter_to("'2023-01-01'^^xsd:date")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('pil_uren_tot')
    test.verify_header_present('pnil_uren_tot')
    test.verify_header_present('percentage_pnil')


def test_if_number_of_rows_returned_is_correct_for_query_1_4_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')
    # test.set_quarter_to("'2023-01-01'^^xsd:date")

    # Assertions
    test.verify_row_count(2)


def test_if_indicator_has_correct_value_for_query_1_4_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.1.rq')
    # test.set_quarter_to("'2022-04-01'^^xsd:date")

    # Assertions
    test.verify_value("pil_uren_tot","4.0", where_condition=("vestiging", "000001254"))

def test_if_dates_can_change_for_query_1_4_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

    # test.set_quarter_to("'2024-01-01'^^xsd:date")

    # Assertions
    test.verify_value("percentage_pnil",None)

# Tests using generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_1_4_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        test.verify_value("percentage_pnil",None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_1_4_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        test.verify_value("percentage_pnil","0", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_1_4_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        test.verify_value("percentage_pnil",None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_1_4_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK)
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        test.verify_value("percentage_pnil","0", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_1_4_0_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK (8u inzet, Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        # test.verify_value("percentage_pnil","0", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        # test.verify_value("percentage_pnil","0", where_condition=("vestiging", "000001287"))
        test.verify_value("pil_uren_tot","80", where_condition=("vestiging", "000001287"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_1_4_0_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK + INH OK (8u + 8u))
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        test.verify_value("percentage_pnil","50.0", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c

def test_if_value_returned_is_correct_for_query_1_4_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK (8u + 8u))
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        test.verify_value("percentage_pnil","100", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d

def test_if_value_returned_is_correct_for_query_1_4_0_04_d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel UTZ OK + Wel INH OK (8u + 8u))
        VWS Beleidsontwikkeling & -monitoring 1.4.0 Percentage ingezette uren personeel niet in loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.4.5.rq')

        # Change the reference dates of the query
        # test.set_quarter_to("'2024-01-01'^^xsd:date")

        # Verify actual result of the query
        test.verify_value("percentage_pnil","100", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()