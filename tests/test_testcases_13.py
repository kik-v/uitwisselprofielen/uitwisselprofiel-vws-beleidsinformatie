from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2024-07-01

#Opmerkingen:
# 

#Testcases 

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "location": "Locatie_De_Beuk_1",                      
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_01_a = [
    {
        "Description": "Testcase 01_a (Geen ZVL functie + Wel AOK (Niet BPT))",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                          
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK Bepaaldetijd (12m))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]  

td_03 = [ 
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK (Niet BPT))",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK Bepaaldetijd (12m))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_04_a = [ 
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK Onbepaaldetijd (12m))",
        "Amount": 10, #Indicator score: 0 (0/19)
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK Bepaaldetijd (Niet op peildatum))",
        "Amount": 10, #Indicator score: 0 (Peildatum niet in AOK)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "location": "Locatie_De_Beuk_1",                       
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel AOK Bepaaldetijd (Vest. 1287))",
        "Amount": 10, #Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_04_d = [ 
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel AOK Onbepaaldetijd (Vest. 1287))",
        "Amount": 10, 
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd)",
        "Amount": 10, # Indicator score: 68.965517241379310344827586 (20/29)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK BPT Niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   
 

td_05_a = [
    {
        "Description": "Testcase 05a (Wel ZVL functie + Wel AOK (12m) - Bepaaldetijd + Onbepaaldetijd)",
        "Amount": 10, # Indicator score: 0 
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    },
                ],
                "ArbeidsOvereenkomstOnbepaaldeTijd": [    
                    {
                        "function": [
                            {
                                "label": "AOK BPT Niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   
 

td_05_b = [
    {
        "Description": "Testcase 05b (Wel ZVL functie + Wel AOK (12m) - 2 Onbepaaldetijd)",
        "Amount": 10, #
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK BPT Niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",                         
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]


# Static Tests
def test_if_headers_are_correct_for_query_1_3_0(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

    test.set_reference_date_to("2022-01-01")

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('aantal_bepaalde_tijd')
    test.verify_header_present('totaal')
    test.verify_header_present('percentage_bepaalde_tijd')

def test_if_number_of_rows_returned_is_correct_for_query_1_3_0(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

    test.set_reference_date_to("2022-01-01")

    # Assertions
    test.verify_row_count(3)


def test_if_indicator_has_correct_value_for_query_1_3_0(db_config):
    """ Test of de indicator de juiste waarde heeft
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')
    test.set_reference_date_to("2022-01-01")

    # Assertions
    test.verify_value("percentage_bepaalde_tijd","11.111111111111111111111100",where_condition=("vestiging", "Totaal Dummy Zorg B.V."))


def test_if_dates_can_change_1_3_0(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

    test.set_reference_date_to("2022-07-01")

    # Assertions
    # test.verify_value("totaal", "6", where_condition=("vestiging","000001254"))
    # test.verify_value("totaal", "1", where_condition=("vestiging","000001287"))
    test.verify_value("totaal", "7", where_condition=("vestiging", "Totaal Dummy Zorg B.V."))

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_3_0_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        test.verify_value("totaal", "8",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_1_3_0_01_a(db_config):
    """ Testcase 01a (Geen ZVL functie + Geen AOK)
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        test.verify_value("totaal", "8",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_1_3_0_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        test.verify_value("totaal", "18",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_1_3_0_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK (BPT))
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        test.verify_value("totaal", "8",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_1_3_0_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK Bepaaldetijd (12m))
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "10",where_condition=('vestiging',"000001254"))
        # test.verify_value("totaal", "7",where_condition=('vestiging',"000001254"))
        test.verify_value("percentage_bepaalde_tijd", "58.823529411764705882352900",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        # test.verify_value("totaal", "1",where_condition=('vestiging',"000001287"))
        # test.verify_value("totaal", "18",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04_a
def test_if_value_returned_is_correct_for_query_1_3_0_04a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK Onbepaaldetijd (12m))
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        test.verify_value("totaal", "17",where_condition=('vestiging',"000001254"))
        # test.verify_value("percentage_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        # test.verify_value("totaal", "18",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04_b
def test_if_value_returned_is_correct_for_query_1_3_0_04b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK Bepaaldetijd (Niet op peildatum))",
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        test.verify_value("totaal", "18",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_1_3_0_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK Bepaaldetijd (Vest. 1287)
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "10",where_condition=('vestiging',"000001254"))
        # test.verify_value("totaal", "7",where_condition=('vestiging',"000001254"))
        # test.verify_value("percentage_bepaalde_tijd", "x",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "10",where_condition=('vestiging',"000001287"))
        # test.verify_value("totaal", "11",where_condition=('vestiging',"000001287"))
        test.verify_value("percentage_bepaalde_tijd", "90.909090909090909090909100",where_condition=('vestiging',"000001287"))
        # test.verify_value("totaal", "18",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04d
def test_if_value_returned_is_correct_for_query_1_3_0_04_d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel AOK Onbepaaldetijd (Vest. 1287))
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("totaal", "7",where_condition=('vestiging',"000001254"))
        # test.verify_value("percentage_bepaalde_tijd", "0",where_condition=('vestiging',"000001254"))
        # test.verify_value("aantal_bepaalde_tijd", "0",where_condition=('vestiging',"000001287"))
        test.verify_value("totaal", "11",where_condition=('vestiging',"000001287"))
        # test.verify_value("totaal", "8",where_condition=('vestiging',"Totaal Dummy Zorg B.V."))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_1_3_0_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd)
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_bepaalde_tijd","10", where_condition=("vestiging", "000001254"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_1_3_0_05_a(db_config):
    """ Testcase 05a (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd + Onbepaaldetijd)
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        test.verify_value("aantal_bepaalde_tijd","10", where_condition=("vestiging", "000001254"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_1_3_0_05_b(db_config):
    """ Testcase 05b (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Onbepaaldetijd + Bepaaldetijd)
        VWS Beleidsontwikkeling & -monitoring 1.3.0. Percentage arbeidsovereenkomsten voor bepaalde tijd op een peildatum
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.3.0.rq')

        # Change measuring period parameters of query
        test.set_reference_date_to("2024-07-01")

        # Verify actual result of the query
        # test.verify_value("aantal_bepaalde_tijd","0", where_condition=("vestiging", "000001254"))
        test.verify_value("totaal","17", where_condition=("vestiging", "000001254"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
