---
title: Algemeen
weight: 2
---
# Algemeen

## Doel van de uitvraag
Op landelijk niveau een beeld kennen van de sector in het kader van beleidsvorming en beleidsmonitoring. Trendanalyse naar de ontwikkeling van de sector ten behoeve van beleidsontwikkeling.