---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.0.1-acc |
| Versie | 1.0 versie 0.9 concept, ter vaststelling in het Tactisch Overleg |
| Doel | Release 1.0 betreft een eerste opzet van het uitwisselprofiel gebaseerd op de inhoud van andere uitwisselprofielen binnen KIK-V tot dit moment. |
| Doelgroep | Ministerie van Volksgezondheid, wetenschap en sport; Zorgaanbieders verpleeghuiszorg; Zorgkantoren. |
| Totstandkoming | De ontwikkeling van deze versie van het uitwisselprofiel is uitgevoerd door het programma KIK-V in overleg met vertegenwoordiging van het ministerie van VWS. Release 1.0 wordt in 2023 vastgesteld door de Ketenraad KIK-V op basis van versie 0.9. |
| Operationeel toepassingsgebied | Beleidsinformatie |
| Status | 	Eerste concept met functionele uitwerking ter vaststelling door Tactisch overleg |
| Functionele scope | Release 1.0 omvat: de beleidsvragen op de onderwerpen personeel en capaciteit |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |
