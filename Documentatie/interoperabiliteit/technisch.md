---
title: Technische interoperabiliteit
weight: 4
---
Indien een zorgaanbieder kan berekenen en aanleveren via een datastation, dan kunnen de antwoorden worden aangeleverd middels de KIK-starter. Voor meer informatie [klik hier](https://www.kik-v.nl/onderwerpen/producten).

Voor aanlevering middels de KIK-starter gelden de volgende afspraken en nadere fasering voor dit uitwisselprofiel:

* Het ministerie van VWS kan een regionaal en landelijk beeld samenstellen door de zorgkantoren te bevragen. Zorgkantoren kunnen de aan hen aangeleverde antwoorden op basis van de SPARQLs in het uitwisselprofiel voor de zorgkantoren gebruiken om deze beleidsinformatie te verstrekken.
* Gedurende verslagjaar 2023 kunnen zorgaanbieders (ook) antwoorden aanleveren via de specifieke SPARQLs voor dit uitwisselprofiel aan de KIK-starter. De KIKstarter maakt na elk kwartaal van de aangeleverde gegevens een landelijk beeld of VWS maakt zelf van de aangeleverde gegevens een landelijk beeld.
* NTB: Welke manier(en) van gegevensuitwisseling wordt/worden gehanteerd voor de terugkoppeling door de afnemer. Deze afspraken worden uitgewerkt nadat is uitgewerkt hoe de implementatie van het uitwisselprofiel verder vorm krijgt.