---
title: Semantische interoperabiliteit
weight: 3
---
## Identificerende vragen

| **Informatievragen:**                          | **Aggregatieniveau** |
|------------------------------------------------|----------------------|
| 0.1 Wat is de naam van de organisatie?         | Organisatieniveau    |
| 0.2 Wat is het KvK-nummer van de organisatie?  | Organisatieniveau    |
| 0.3 Wat is het KvK-nummer van de vestiging(en) | Vestigingsniveau     |

**Doel en achtergrond**
* VWS wil op landelijk niveau inzicht in ontwikkelingen. Om op landelijk niveau de informatie te kunnen samenstellen, worden de antwoorden verzameld op zowel vestigingsniveau als op totaal organisatieniveau en vervolgens op landelijk geabstraheerd/ gemiddeld.
* De algemene gegevens zijn nodig om de juiste doelgroep te bepalen en samen te stellen.

### Personeel

| **VWS 1.1 Wat is het aantal personeelsleden op een peildatum? (ZK 1.2)**                                                                 |
|----------------------------------------------------------------------------------------------------------------------------------------------|
| **VWS 1.2 Hoeveel zorgmedewerkers per kwalificatieniveau zijn werkzaam bij een vestiging op dit moment (exclusief overhead)? (IGJ01/1.2.3)** |
| **VWS 1.3 Wat is het percentage personeel met een arbeidsovereenkomst voor bepaalde tijd op peildatum? (ZK 3.1)**                            |
| **VWS 1.4 Wat is het percentage ingezette uren personeel niet in loondienst (PNIL)? (ZK 8.1)**                                               |
| **VWS 1.5 Wat is het aantal vrijwilligers? (ZK 9.1)**                                                                                        |
| **VWS 1.6 Wat is het aantal leerlingen? (ZK 10.1)**                                                                                          |
| **VWS 1.7 Wat is het aantal verloonde uren ? (ZK 2.2)**                                                                                      |
| **VWS 2.1 Wat is de leeftijdsopbouw van het personeel? (ZK 5.1)**                                                                            |
| **VWS 3.1 Wat is het kortdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)? (ZK 11.1)**                                             |
| **VWS 3.2 Wat is het kortdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)? (ZK 11.2)**                                             |
| **VWS 3.3 Wat is het langdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)? (ZK 11.3)**                                             |
| **VWS 3.4 Wat is het langdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)? (ZK 11.4 )**                                            |
| **VWS 4.1 Wat is het percentage instroom personeel? (ZK 13.1)**                                                                              |
| **VWS 4.2 Wat is het percentage uitstroom personeel? (ZK 13.2)**                                                                             |
| **VWS 4.3 Wat is het percentage doorstroom personeel naar oplopend kwalificatieniveau? (ZK 13.3)**                                           |
| **VWS 4.4 Wat is het percentage doorstroom personeel naar aflopend kwalificatieniveau? (ZK 13.4)**                                           |

**Aggregatieniveau**
Het aggregatieniveau is voor alle indicatoren **organisatie- en vestigingsniveau**.

**Doel en achtergrond**
* VWS wil inzicht in o.a. personeelsopbouw, verloop en ziekteverzuim, omdat de ontwikkelingen op de arbeidsmarkt van groot belang zijn voor kwaliteit van zorg en het vormen van beleid op dit onderwerp.
* Voor VWS is het van belang om te weten of zorgaanbieders het leveren van zorg kunnen blijven garanderen of dat er actie moet worden ondernomen.
* VWS wil trendanalyses kunnen doen voor beleidsvorming en beleidsmonitoring van de arbeidsmarkt in de verpleeghuissector. 

### Cliënt

| **VWS 5.1 Wat is het aantal cliënten per zorgprofiel op peildatum? (ZK 14.1)** |
|------------------------------------------------------------------------------------|
| **VWS 5.2 Wat is het aantal cliënten per leveringsvorm op peildatum? (ZK 14.2)**   |

**Aggregatieniveau**
Het aggregatieniveau is voor alle indicatoren **organisatie- en vestigingsniveau**.

**Doel en achtergrond**
* VWS wil trendanalyses kunnen doen voor beleidsvorming en beleidsmonitoring van de verpleeghuissector en daarvoor is informatie over het aantal, de leveringsvorm en type cliënten van belang.

### Capaciteit

| **VWS 7.1 Wat is het aantal wooneenheden per vestiging op een peildatum?** |
|--------------------------------------------------------------------------------|
| **VWS 7.2 Wat is het aantal personen dat per vestiging kan wonen (capaciteit)?** |

**Aggregatieniveau**
Het aggregatieniveau is voor alle indicatoren **organisatie- en vestigingsniveau**.

**Doel en achtergrond**
* VWS wil inzicht in de capaciteit van de sector om te kijken om de (verwachte) schaarste in beeld te hebben. Om hier vervolgens beleid op te kunnen maken.


## Algemene uitgangspunten
Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel.

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten:

[Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)


## Gevalideerde vragen
De functionele beschrijving van de berekening per gevalideerde vraag staat hier: [klik hier](/Gevalideerde_vragen_functioneel)

Berekening van de informatievragen vindt plaats onder verantwoordelijkheid van de zorgaanbieder.

Op alle indicatoren kunnen extra verdeelsleutels worden toegepast: één voor de verdeling per financieringsstroom (Wlz, Zvw, Wmo) en één voor de verdeling in langdurige zorgsector (Wlz VV, GGZ-B, LG, LVG, VG, ZGaud en ZGvis). [Klik hier](https://kik-v-publicatieplatform.nl/documentatie/Verdeelsleutels/) voor de verdeelsleutels.

## Benodigde gegevenselementen
De concepten, eigenschappen en relaties die nodig zijn om de indicatoren VWS te beantwoorden staan hier: [klik hier](/Gevalideerde_vragen_technisch/Modelgegevensset).

## Aggregatieniveau
VWS baseert zich op de vragen die zorgkantoren in het eigen uitwisselprofiel hebben opgenomen. De antwoorden op deze vragen worden steeds zowel op vestigingsniveau als op het totale organisatieniveau gegeven. Deze manier van werken wordt in het uitwisselprofiel van VWS 1 op 1 overgenomen. Dit stelt VWS in staat om zowel regionale als landelijke beelden op te stellen, gebaseerd op de optelsommen van informatie van zowel vestigingen als van organisaties.