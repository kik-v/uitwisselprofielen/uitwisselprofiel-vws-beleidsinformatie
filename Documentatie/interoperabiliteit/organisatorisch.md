---
title: Organisatorische interoperabiliteit
weight: 2
---
## Aanlevering
Voor VWS is het beschikbaar maken van gegevens op dagbasis niet nodig om trendanalyses te kunnen doen; gegevens op jaarbasis is te weinig frequent. Om deze reden is gekozen voor cijfers op kwartaalbasis. De wens van VWS is om hiermee zoveel mogelijk aan te sluiten bij de gewenste frequentie van zorgkantoren die weer de interne planning- en control cyclus van zorgaanbieders volgen. 

Voor dit uitwisselprofiel betekent dit dat de resultaten soms op een peildatum per kwartaal en soms over een kwartaal beschikbaar te zijn. Afhankelijk van de interne processen bij de zorgaanbieders zijn deze minimaal 1 dag na de gevraagde meetperiode (dag, kwartaal) beschikbaar. Per vraag is bekeken welke peildatum of peilperiode van een kwartaal wordt gehanteerd: zie Semantische laag voor verdere details.

## Looptijd
De looptijd van het uitwisselprofiel is continu doorlopend tot het moment van wijziging.

## Gewenste bewaartermijn
VWS zal zelf de antwoorden op regionaal en landelijk niveau bewaren ten behoeve van trendanalyses.

## Afspraken bij twijfels over de kwaliteit van gegevens
NTB Afspraken over nalevering of correctie. Deze afspraken worden uitgewerkt nadat is uitgewerkt hoe de implementatie van het uitwisselprofiel verder vorm krijgt. Eea in relatie tot hetgeen zorgkantoren aan afspraken over terugkoppeling hebben gemaakt.

## Terugkoppeling
NTB Afspraken terugkoppeling over de aangeleverde antwoorden en wat daarmee is gedaan. Deze afspraken worden uitgewerkt nadat is uitgewerkt hoe de implementatie van het uitwisselprofiel verder vorm krijgt. Eea in relatie tot hetgeen zorgkantoren aan afspraken over terugkoppeling hebben gemaakt.

NTB Onderling contact over niet kunnen aanleveren of problemen in de aanlevering. Deze afspraken worden uitgewerkt nadat is uitgewerkt hoe de implementatie van het uitwisselprofiel verder vorm krijgt.

## In- en exclusiecriteria
De uitvraag is bedoeld voor organisaties die onder onderstaande inclusiecriteria vallen:

Doelgroep:
* Tot de doelgroep behoren alle instellingen die een Wlz contract hebben met een zorgkantoor.

Zorgprofiel/ cliëntgroep:
* De profielen conform Wet Langdurige zorg worden geïncludeerd.