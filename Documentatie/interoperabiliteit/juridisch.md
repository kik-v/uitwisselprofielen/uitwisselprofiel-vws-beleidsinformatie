---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag
Artikel 9.2.1 Wet Langdurige Zorg (zie ook [Juridisch kader](https://kik-v-publicatieplatform.nl/afsprakenset))

1. Bij of krachtens algemene maatregel van bestuur kunnen in het belang van de zorgverlening, de bekostiging daarvan en de afstemming op andere wettelijke voorzieningen, regels worden gesteld over de kosteloze verstrekking van informatie van beleidsmatige en beheersmatige aard:

      a. door zorgaanbieders aan Wlz-uitvoerders, de zorgautoriteit en Onze Minister,

      b. door Wlz-uitvoerders aan de zorgautoriteit en Onze Minister,
      
      c. door het CIZ aan Onze Minister.

2. De bij of krachtens algemene maatregel van bestuur te stellen regels als bedoeld in het eerste lid, hebben geen betrekking op persoonsgegevens als bedoeld in de Algemene verordening gegevensbescherming en worden niet gesteld dan nadat met door zorgaanbieders of de Wlz-uitvoerders voorgedragen koepelorganisaties, overleg is gevoerd over de inhoud van de in het eerste lid bedoelde gegevens en standaardisering van de wijze waarop de gegevens worden verstrekt.
3. Onverminderd het bepaalde in het tweede lid, worden voor de verstrekking van informatie door het CIZ aan Onze Minister, bedoeld in het eerste lid, onderdeel c, door het CIZ persoonsgegevens, waaronder gegevens over de gezondheid, als bedoeld in artikel 4, onderdeel 15, van de Algemene verordening gegevensbescherming verwerkt.
4. Bij of krachtens algemene maatregel van bestuur kan worden bepaald dat het overleg, bedoeld in het tweede lid, ook plaatsvindt met andere organisaties en instanties dan genoemd in het tweede lid, en kan worden bepaald dat het eerste en tweede lid ook van toepassing is ten aanzien van die organisaties en instanties.
