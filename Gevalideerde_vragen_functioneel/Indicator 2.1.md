---
title: 2.1 Leeftijdsopbouw personeel in loondienst
description: "Deze indicator betreft per kwartaal de leeftijdopbouw van het personeel." 
weight: 5
---
## Indicator

**Definitie:** Deze indicator betreft per kwartaal de leeftijdopbouw van het personeel in loondienst.

**Teller:** Aantal personen per leeftijdscategorie.

**Noemer:** Niet van toepassing.

## Toelichting
Deze indicator betreft per kwartaal de leeftijdopbouw van het personeel in loondienst.


## Uitgangspunten

* Alle personeelsleden in loondienst worden geïncludeerd.
* De indeling van een persoon in een leeftijdcategorie (van tijdvakken van 5 jaren) wordt bepaald op basis van de leeftijd van de persoon op de laatste dag van het kwartaal.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle arbeidsovereenkomsten in het kwartaal.
2. Bepaal (o.b.v. de geboortedatum in de arbeidsovereenkomst) voor elke arbeidsovereenkomst uit stap 1 de leeftijd van de persoon op de laatste dag van het kwartaal.
3. Bereken o.b.v. stap 2 voor de organisatie het aantal arbeidsovereenkomsten per leeftijdcategorie.

**Kwartaal: mm-jj t/m mm-jj**
| Leeftijdscategorie: | 16 - 20 | 21 - 25 | 26 - 30 | ... | 71 - 75 | 
|----------------|--------|-----------|-----------|-----------|-----------|
| Organisatie | Stap 3 | Stap 3 |  Stap 3 | Stap 3 | Stap 3 | 
