---
title: 1.2 Wat is het aantal werknemers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst?
description: "Aantal werknemers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst op een peildatum." 
weight: 4
---

## Indicator

**Definitie:** Aantal werknemers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst op een peildatum.

**Teller:** Aantal werknemers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal werknemers (en het percentage t.o.v. totaal) met een zorgverlener functie per kwalificatieniveau per soort werkovereenkomst bij een organisatie op een peildatum. De indicator wordt per kwalificatieniveau en voor alle kwalificatieniveaus samen berekend.


## Uitgangspunten

* Alle werknemers met een of meerdere zorgverlener functies op de peildatum worden geïncludeerd.
* Of een werknemer een zorgverlener functie heeft, wordt bepaald op basis van de functie in de werkovereenkomst.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer van de betreffende organisatie op de peildatum van alle werknemers de werkovereenkomsten met een zorgverlener functie.
2. Bepaal op basis van de functie per werkovereenkomst uit stap 1 het kwalificatieniveau.
3. Bereken op basis van stap 2 per kwalificatieniveau het totaal aantal werknemers en per kwalificatieniveau het aantal werknemers per soort werkovereenkomst.
4. Bereken op basis van stap 3 het totaal aantal werknemers per soort werkovereenkomst.
5. Bereken het percentage aan werknemers per soort werkovereenkomst.

Peildatum: dd-mm-jjjj

| Kwalificatieniveau | Aantal werknemers met een werkovereenkomst voor onbepaalde tijd met een zorgverlener functie | Aantal werknemers met een werkovereenkomst voor bepaalde tijd met een zorgverlener functie | Aantal werknemers met een oproepovereenkomst met een zorgverlener functie | Aantal werknemers met een werkovereenkomst BBL met een zorgverlener functie | Aantal werknemers met een inhuurovereenkomst met een zorgverlenerfunctie | Aantal werknemers met een uitzendovereenkomst met een zorgverlenerfunctie | Aantal werknemers met een stageovereenkomst met een zorgverlenerfunctie | Aantal werknemers met een vrijwilligersovereenkomst met een zorgverlenerfunctie | Totaal |
|---|---|---|---|---|---|---|---|---|---|
| 0/1 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| 2 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| N | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| Totaal aantal werknemers | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | Stap 4 | 
| % totaal aantal werknemers | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | Stap 5 | 100% | 


